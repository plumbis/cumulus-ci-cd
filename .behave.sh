#!/usr/bin/env bash

# The reason .behave.sh is used, is because when `vagrant up`
# is run, it creates a .vagrant file to track the VMs
# However when gitlab runs, if the task fails (i.e., behave)
# gitlab immediately halts and deletes any created files (i.e., .vagrant dir)
# So as a result, if behave fails you can't destroy the VMs
# To address this the bash script handles this.

vagrant up --color
sleep 5
script -q /dev/null behave validation
exit_code=$?
vagrant destroy -f
exit $exit_code
